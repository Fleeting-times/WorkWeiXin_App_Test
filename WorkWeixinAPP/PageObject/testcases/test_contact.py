import pytest
import yaml

from WorkWeixinAPP.PageObject.page.app import App

with open('../datas/addcontacts.yml', encoding='utf-8') as f:
    datas = yaml.safe_load(f)

with open('../datas/delcontacts.yml', encoding="utf-8") as f:
    del_datas = yaml.safe_load(f)

class TestContast:

    def setup_class(self):
        """
        这个函数的功能包括启动APP
        :return:
        """

        # 创建一个App示例
        self.app = App()

        # 调用启动App的方法,然后调用“进入首页”的方法
        self.mian = self.app.start().goto_main()


    # def test_dk(self):
    #     dk_page = self.mian.goto_workbench().daka_click().go_out_Attendance().DK_Click()
    #
    #     #验证是否打卡成功给
    #     result = dk_page.dk_result()
    #
    #     assert "打卡成功" in result
    #
    #     self.app.back()

    def teardown_class(self):
        """
        返回
        :return:
        """
        self.app.stop()

    @pytest.mark.parametrize("name, gender, telephone", datas)
    def test_addcontact(self, name, gender, telephone):
        """
        添加联系人
        通过调用每一个方法，实现类似页面跳转的方法，与APP中点击每一个功能呈现的页面跳转一致
        :return:
        """
        my_page = self.mian.goto_contactlist().add_contact().\
            add_manully().set_name(name).\
            set_gender(gender).set_telephone(telephone).click_save()

        #验证是否添加成功
        text = my_page.get_toast()

        assert "成功" in text

        self.app.back()

    @pytest.mark.parametrize("name", del_datas)
    def test_delete_contact_people(self, name):
        """
        删除联系人
        :return:
        """
        #删除之前名字的数量
        number_first = self.mian.goto_contactlist().search_contact().Seatch_First_Information_Number(name)
        if number_first == 0:
            print("该联系人不在通讯录中")

        else:
            #删除之后，名字的数量
            number_end = self.mian.goto_contactlist().search_contact().Search_Information(name).Select_Edit().Edit_Member().delete_member().confirm_delete_member().Validation_Rresults(name)

            assert (number_first-number_end)==1

            print(f"删除前有{number_first}个人，删除后有{number_end}个人, 此次删除了{number_first-number_end}人")







    
