from appium.webdriver.common.mobileby import MobileBy

from WorkWeixinAPP.PageObject.page.basepage import BasePage
from WorkWeixinAPP.PageObject.page.contactlistpage import ContactListPage
from WorkWeixinAPP.PageObject.page.workbenchpage import WorkBenchPage


class MainPage(BasePage):
    """
    主页面，底部有四个元素，分别是”消息“、”通讯录“、”工作台“、”我“
    """

    contactlist = (MobileBy.XPATH, "//*[@resource-id='com.tencent.wework:id/e48' and @text='通讯录']")
    workbench = (MobileBy.XPATH, "//*[@resource-id='com.tencent.wework:id/e48' and @text='工作台']")

    def goto_contactlist(self):
        """
        点击通讯录
        进入到通讯录页面
        :return:
        """
        # 点击跳转到通讯录页面
        self.find_and_click(self.contactlist)

        return ContactListPage(self.driver)

    def goto_workbench(self):
        """
        点击工作台
        进入到工作台页面
        :return:
        """
        self.find_and_click(self.workbench)

        return WorkBenchPage(self.driver)

