from appium.webdriver.common.mobileby import MobileBy

from WorkWeixinAPP.PageObject.page.basepage import BasePage
from WorkWeixinAPP.PageObject.page.click_in_successfullypage import Click_in_SuccessfullyPage


class Go_Out_DK_Page(BasePage):
    """
    此页面是“外出打卡”的详情页
    """
    dk_click_element = (MobileBy.XPATH, "//*[contains(@text, '次外出')]")

    def DK_Click(self):
        # 点击打卡，使用contains方法，定位text中包含关键字“次外出”的也页面元素
        self.find_and_click(self.dk_click_element)

        return Click_in_SuccessfullyPage(self.driver)
