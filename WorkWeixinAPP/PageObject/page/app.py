"""
存放和APP相关的功能
例如  启动APP   关闭APP   停止APP   进入APP首页
"""
from appium import webdriver

from WorkWeixinAPP.PageObject.page.basepage import BasePage
from WorkWeixinAPP.PageObject.page.mainPage import MainPage


class App(BasePage):
    def start(self):
        """
        启动APP
        判断driver是否存在
        如果存在，直接启动app即可
        #第一次调用start得时候，driver的值为None
        :return:
        """
        if self.driver == None:
            desired_caps = {}
            desired_caps['platformName'] = 'Android'
            desired_caps['platformVersion'] = '6.0'
            desired_caps['deviceName'] = '127.0.0.1:7555'
            desired_caps['appPackage'] = 'com.tencent.wework'
            desired_caps['appActivity'] = 'com.tencent.wework.launch.WwMainActivity'
            desired_caps['noReset'] = 'True'
            desired_caps['shipServerInstallation'] = 'True'  # 跳过uiautomator2 server的安装
            desired_caps['skipDeviceInitialization'] = 'True'  # 跳过设备的初始化
            desired_caps['dontStopAppOnReset'] = 'True'  # 启动前不停止APP
            desired_caps['settings[waitForIdleTimeout]'] = 0  # 避免有些无意义的动态页面不断的刷新，造成程序等待浪费时间

            self.driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub", desired_caps)
        else:
            #launch_app()这个方法，不需要传入任何参数，会自动读取desired_caps中定义的Activity,启动其中定义的APP
            self.driver.launch_app()
        self.driver.implicitly_wait(30)  # 设置隐式等待

        return self  # 返回到当前的实例中

    def restart(self):
        """
        重启APP
        :return:
        """
        self.driver.close() #关闭APP
        self.driver.launch_app()    #启动APP
        return self

    def stop(self):
        """
        停止APP运行
        :return:
        """
        self.driver.quit()

    def goto_main(self):
        """
        进入首页
        :return:
        """
        return MainPage(self.driver)
