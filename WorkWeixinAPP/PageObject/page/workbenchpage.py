"""
工作台页面
"""
from WorkWeixinAPP.PageObject.page.attendancepage import AttendancePage
from WorkWeixinAPP.PageObject.page.basepage import BasePage


class WorkBenchPage(BasePage):
    """
    本页面暂时会用到“打卡”功能
    """

    #定义滑动查找的目标关键字
    daka_test = "打卡"

    def daka_click(self):

        """
        打卡功能
        需要在此页面，互动到“打卡”按钮可见
        然后点击打卡，跳转到打卡详情页
        :return:
        """
        self.find_scroll(self.daka_test).click()

        #返回到“打开页面”
        return AttendancePage(self.driver)
