from appium.webdriver.common.mobileby import MobileBy

from WorkWeixinAPP.PageObject.page.Confirm_Delete_Page import Confirm_Delete_Member
from WorkWeixinAPP.PageObject.page.basepage import BasePage
"""
本页面功能是编辑成员信息
"""

class Edit_Member_Information(BasePage):

    """
    删除成员
    """
    delete_click = (MobileBy.XPATH, "//*[@text='删除成员']")

    def delete_member(self):
        # 点击删除成员
        self.find_and_click(self.delete_click)
        return Confirm_Delete_Member(self.driver)


