from appium.webdriver.common.mobileby import MobileBy

from WorkWeixinAPP.PageObject.page.basepage import BasePage
from WorkWeixinAPP.PageObject.page.go_out_DK_page import Go_Out_DK_Page


class AttendancePage(BasePage):
    """
    本页面是打卡详情页
    分为两个功能，一个是“上下班打卡”，另一个是“外出打卡”
    """
    go_out_element = (MobileBy.ID, 'com.tencent.wework:id/h58')

    def go_out_Attendance(self):
        # 外出打卡功能
        # 点击外出打开，进入外出打卡界面
        self.find_and_click(self.go_out_element)
        return Go_Out_DK_Page(self.driver)

    def toWork_and_offWork_Attendance(self):
        # 上下班打卡功能
        pass
