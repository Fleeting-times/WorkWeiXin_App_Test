# 企业微信APP自动化测试

#### 介绍
本仓库中，存放着学习Appium自动化过程中的实战项目，测试软件是企业微信APP，主要测试了“添加成员”、“删除成员”和“外出打卡”三个功能，运用了PageProject得测试思想和数据驱动等方法，完成了项目实战


#### 安装教程
1.  安装java8环境
2.  安装Android SDK
3.  修改build-tools版本为29
4.  安装Appium
5.  安装Nodejs，并通过npm安装Appium server
6.  安装mumu模拟器


